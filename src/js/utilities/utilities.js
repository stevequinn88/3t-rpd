var waypoints = require("../../../node_modules/waypoints/lib/noframework.waypoints.js");

const utilities = {};

utilities.setupNavWaypoint = () => {
    const waypoint = new Waypoint({
        element: document.getElementById('main'),
        handler: function(direction) {
            if (direction == 'down')
                document.querySelector('.header').classList.add('header--active');
            else {
                document.querySelector('.header').classList.remove('header--active');
            }
        }
    });
};

module.exports = utilities;
