
module.exports = function(grunt) {

  // measure the time
  require('time-grunt')(grunt);
  // load the tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });

  var config = {
    // Minify the scripts and css for production
    minifyScripts: false,
    inlineCss: false
  };

  // Development Url for Browser sync proxy
  //var devUrl = "vagrant-dev.filmstro.io:8080/";

  // CONFIG
  grunt.initConfig({
    config: config,
    pkg: grunt.file.readJSON('package.json'),

    connect: {
      options: {
        port: 3000,
        open: true,
        useAvailablePort: true
      },
      server: {
        options: {
          base: 'dist',
          logger: 'dev',
          hostname: 'localhost'
        }
      }
    },

    'ftp-deploy': {
      build: {
        auth: {
          host: 'acai.active-ns.com',
          port: 21,
          authKey: 'key1'
        },
        src: 'dist/',
        dest: '/',
        exclusions: ['dist/**/.DS_Store', 'dist/**/Thumbs.db', 'dist/.tmp']
      }
    },

    liquid: {
      options: {
        includes: 'src/liquid',
        pages: grunt.file.readJSON('data/pages.json'),
        dateNow: '<%= grunt.template.date( Date.now(), "dddd, mmmm dS, yyyy, h:MM:ss TT" ) %>',
        yearNow: '<%= grunt.template.date( Date.now(), "yyyy" ) %>',
        //example data
        list: [
          { item: "Item" },
          { item: "Item" },
          { item: "Item" },
          { item: "Item" },
          { item: "Item" },
          { item: "Item" }
        ]
      },
      dev: {
        files: [
          {
            expand: true,
            cwd: 'src/liquid',
            src: ['**/*.liquid', '!includes/*.liquid'],
            dest: 'dist',
            ext: '.html'
          }
        ]
      },
      prod: {
        files: [
          {
            expand: true,
            cwd: 'src/liquid',
            src: ['**/*.liquid', '!includes/*.liquid'],
            dest: 'dist',
            ext: '.html'
          }
        ],
        options: {
          minify: config.minifyScripts
        }
      }
    },

    // LibSass
    sass: {
      dev: {
        options: {
          // For some reason, nested breaks source map functionality
          outputStyle: 'expanded',
          sourceMap: true
        },
        files: [{
          expand: true,
          cwd: 'src/css',
          src: '*.scss',
          dest: 'dist/assets/css',
          ext: '.css'
        }]
      },
      prod: {
        options: {
          outputStyle: 'nested',
          sourceMap: false
        },
        files: [{
          expand: true,
          cwd: 'src/css',
          src: '*.scss',
          dest: 'dist/assets/css',
          ext: '.css'
        }]
      },
      styleguide: {
        options: {
          outputStyle: 'compressed'
        },
        files: [{
          expand: true,
          cwd: 'src/styleguide',
          src: ['*.{scss,sass}'],
          dest: 'dist/styleguide/css',
          ext: '.css'
        }]
      }
    },

    modernizr: {
      dev: {
        options: [
          "setClasses"
        ],
        dest: 'dist/assets/js/libs/modernizr-custom.js',
        extra: {
          'shiv' :       false,
          'printshiv' :  false,
          'load' :       true,
          'mq' :         false,
          'cssclasses' : true
        },
        extensibility: {
          'addtest':      true,
          'prefixed':     false,
          'teststyles':   false,
          'testprops':    false,
          'testallprops': false,
          'hasevents':    false,
          'prefixes':     false,
          'domprefixes':  false
        },
        crawl: false,
        excludeTests: ['hidden'],
        tests: ( function() {
          var tests = grunt.file.readJSON('./node_modules/modernizr/lib/config-all.json')['feature-detects'];
          // Return first-level tests
          return tests.map(function(str) {
            return str.replace('-','');
          }).concat([
            // Additional tests
            'inlinesvg'
          ]);
        }() )
      },
      prod: {
        cache: true,
        options: [
          "setClasses"
        ],
        dest: 'dist/assets/js/libs/modernizr-custom.js',
        extra: {
          'shiv' :       false,
          'printshiv' :  false,
          'load' :       true,
          'mq' :         false,
          'cssclasses' : true
        },
        extensibility: {
          'addtest':      true,
          'prefixed':     false,
          'teststyles':   false,
          'testprops':    false,
          'testallprops': false,
          'hasevents':    false,
          'prefixes':     false,
          'domprefixes':  false
        },
        files: {
          src: [
            'dist/assets/js/{,*/}*.js',
            'dist/assets/css/{,*/}*.css'
          ]
        },
        uglify: true,
        excludeTests: ['hidden'],
        tests: [
          'pointerevents',
          'touchevents',
          'inlinesvg'
        ]
      }
    },

    postcss: {
      options: {
        processors: [
          require('autoprefixer')()
        ]
      },
      dev: {
        options: {
          map: true
        },
        expand: true,
        cwd:  'dist',
        src:  'assets/css/main.css',
        dest: 'dist'
      },
      prod: {
        expand: true,
        cwd:  'dist',
        src:  ['assets/css/main.css', 'assets/css/style.css' ],
        dest: 'dist'
      },
      styleguide: {
        options: {
          map: false
        },
        expand: true,
        cwd:  'dist/styleguide/css',
        src:  '*.css',
        dest: 'dist/styleguide/css'
      }
    },

    browserify: {
      dist: {
        files: [{
          expand: true,
          //flatten: true,
          cwd:  'src/js',
          src:  '**/*.js',
          dest: 'dist/assets/js'
        }],
        options: {
          transform: ['babelify']
        }
      }
    },

    svgstore: {
      options: {
        prefix : 'icon-', // This will prefix each ID
        svg: { // will add and overide the the default xmlns="http://www.w3.org/2000/svg" attribute to the resulting SVG
          viewBox : '0 0 100 100',
          xmlns: 'http://www.w3.org/2000/svg',
          style: 'display: none'
        }
      },
      default: {
        files: {
          'src/liquid/includes/_icons.liquid': ['src/svg/*.svg']
        }
      }
    },

    eslint: {
      target: ['src/js/main.js']
    },

    concat: {
      options: {
        separator: ';'
      }
    },

    clean: {
      dist: [ 'dist/' ],
      tmp:  [ '.tmp/ '],
      unminified: ( function () {
        if ( config.minifyScripts ) {
          return [
            'dist/assets/css/main.css',
            'dist/assets/js/main.js'
          ];
        } else {
          // Scripts are not minified, nothing to clean
          return [];
        }
      }() ),
      unrevved: [
        'dist/assets/css/main.min.css',
        'dist/assets/css/vendor.min.css',
        'dist/assets/js/main.min.js',
        'dist/assets/js/vendor.min.js'
      ]
    },

    imagemin: {
      prod: {
        files: [{
          expand: true,
          cwd: 'src/img/',
          src: ['**/*.{png,svg,jpg,gif}'],
          dest: 'dist/assets/img'
        }]
      }
    },

    copy: {
      fonts: {
        expand: true,
        cwd: 'src',
        src: 'font/**',
        dest: 'dist/assets/'
      },
      index: {
        dot: true,
        expand: true,
        cwd: 'src/index',
        src: '**/*',
        dest: 'dist'
      },
      images: {
        expand: true,
        cwd: 'src',
        src: 'img/**',
        dest: 'dist/assets/'
      },
      video: {
        expand: true,
        cwd: 'src',
        src: 'video/**',
        dest: 'dist/assets/'
      },
      styleguide: {
        files: [{
          expand: true,
          cwd: 'src/img/site',
          src: ['**/*'],
          dest: 'dist/styleguide/img'
        }]
      }
    },

    useminPrepare: {
      html: 'dist/index.html',
      options: {
        dest: 'dist'
      }
    },

    /*filerev: {
      options: {
        algorithm: 'md5',
        length: 8
      },
      prod: {
        expand: true,
        cwd: 'dist/assets',
        src: ['js/!*.js','css/!*.css'],
        dest: 'dist/assets'
      }
    },*/

    usemin: {
      options: {
        dirs: ['dist']
      },
      html: ['dist/**/*.html'],
      css:  ['dist/assets/css/**/*.css'],
      js:   ['dist/assets/js/**/*.js']
    },

    /*critical: {
      index: {
        options:  {
          base: './',
          css: [],
          ignore: ['@font-face'],
          pathPrefix: '/',
          minify: true,
          width: 1099,
          height: 300
        },
        src: 'dist/index.html',
        dest: 'dist/index.html'
      }
    },*/

    eol: {
      to_lf_replace: {
        options: {
          eol: 'lf',
          replace: true
        },
        files: {
          src: [
            'dist/*.html'
          ]
        }
      }
    },

    favicons: {
      options: {
        tileBlackWhite: false,
        html: 'src/liquid/includes/_favicons.liquid'
      },
      icons: {
        src: 'src/misc/favicon.png',
        dest: 'dist/'
      }
    },

    // Styleguide
    styledown: {
      build: {
        files: {
          'dist/styleguide/index.html': [
            'src/styleguide/config.md',
            'src/css/partials/settings/_settings.local.scss',
            'src/css/partials/base/_base.forms.scss',
            'src/css/partials/objects/*.scss',
            'src/css/partials/components/*.scss',
            'src/css/partials/utilities/*.scss'
          ]
        },
        options: {
          css: function() {
            return config.minifyScripts ? [
              '../assets/css/vendor.min.css',
              '../assets/css/main.min.css',
              'css/styleguide.css'
            ] : [
              '../assets/css/vendor.min.css',
              '../assets/css/main.css',
              'css/styleguide.css'
            ];
          }(),
          js: function() {
            return config.minifyScripts ? [
              '../assets/js/libs/modernizr.min.js',
              '../assets/js/vendor.min.js',
              '../assets/js/main.min.js'
            ] : [
              '../assets/js/libs/modernizr.min.js',
              '../assets/js/vendor.min.js',
              '../assets/js/main.js'
            ];
          }(),
          title: 'Puppy Style Guide'
        }
      }
    },

    concurrent: {
      options: {
        limit: 8
      },
      prod: [
        'sass:prod',
        'liquid:prod',
        'copy:scripts',
        'copy:index',
        'imagemin:prod',
        'copy:fonts'
      ]
    },

    watch: {
      options: {
        livereload: true
      },
      scripts: {
        files: ['src/js/*.js', 'src/js/**/*.js' ],
        tasks: ['eslint', 'browserify']
      },
      fonts: {
        files: ['src/fonts/**'],
        tasks: ['copy:fonts']
      },
      index: {
        files: ['src/index/**'],
        tasks: ['newer:copy:index']
      },
      styles: {
        files: ['src/css/**'],
        tasks: ['sass:dev', 'postcss:dev']
      },
      liquid: {
        files: ['src/liquid/**'],
        tasks: ['liquid:dev']
      },
      images: {
        files: ['src/img/**'],
        tasks: ['newer:copy:images']
      },
      svg: {
        files: ['src/svg/**'],
        tasks: ['svgstore']
      }
    },
    browserSync: {
      server: {

        bsFiles: {
          src : ['dist/assets/css/main.css',
                 'dist/assets/js/main.js',
                 'dist/*.html'
          ]
        },

        options: {
          watchTask: true,
          server: {
            baseDir: ["../3t-rpd", "dist"]
          }
        }
      }
    }

  });

  // TASKS
  grunt.registerTask('minify', function() {
    if (config.minifyScripts) {
      grunt.task.run('useminPrepare');
      grunt.task.run('concat:generated');
      grunt.task.run('cssmin:generated');
      grunt.task.run('uglify:generated');
      grunt.task.run('filerev');
      grunt.task.run('usemin');
      grunt.task.run('cleanUnminified');
      grunt.task.run('clean:unrevved');
    } else {
      grunt.task.run('useminPrepare');
      grunt.task.run('concat:generated');
      grunt.task.run('cssmin:generated');
      grunt.task.run('uglify:generated');
      grunt.task.run('usemin');
      grunt.log.ok('Minification is switched off. Skipping');
    }
  });

  // Custom task to inline CSS
  // We need this to run after filerev to get revved files summary
  // If minification is switched off, unminified styles are inlined
  grunt.registerTask('inlineCss', function() {
    var cssToInline = config.minifyScripts ? [
      grunt.filerev.summary['dist/assets/css/main.min.css']
    ] : [
      'dist/assets/css/main.css'
    ];

    if (config.inlineCss) {
      // grunt.verbose.write( JSON.stringify(grunt.filerev.summary, null, 2) );
      grunt.config('critical.index.options.css', cssToInline);
      grunt.task.run('critical');
    } else {
      grunt.log.ok('CSS inline is switched off. Skipping');
    }
  });

  // Custom unminified files cleaning
  // We need to get the revved files' names
  // and run the clean:unminified task
  /*grunt.registerTask('cleanUnminified', function() {
    var unminifiedAndRevved = [
      'dist/assets/css/main.css',
      'dist/assets/js/main.js',
      grunt.filerev.summary['dist/assets/css/main.css'],
      grunt.filerev.summary['dist/assets/js/main.js']
    ];

    grunt.config('clean.unminified', unminifiedAndRevved);
    grunt.task.run('clean:unminified');
  });*/

  grunt.registerTask('deploy', [
    'ftp-deploy'
  ]);

  grunt.registerTask('server', [
    'connect',
    'browserSync',
    'watch'
  ]);

  grunt.registerTask('dev', [
    'eslint',
    'clean:tmp',
    'clean:dist',
    'modernizr:dev',
    'sass:dev',
    'postcss:dev',
    'browserify',
    'svgstore',
    'eol',
    //'favicons',
    'liquid:dev',
    'copy'
  ]);

  grunt.registerTask('prod', [
    'eslint',
    'clean:dist',
    'svgstore',
    'favicons',
    'concurrent:prod',
    'eol',
    'styleguide',
    'postcss:prod',
    'minify',
    'inlineCss'
  ]);

  // Styleguide compilation task
  grunt.registerTask('styleguide', [
    'styledown',
    'sass:styleguide',
    'postcss:styleguide',
    'copy:styleguide'
  ]);

};
