const lory = require('lory.js').lory;

const comps = {};

comps.setupMobileMenu = () => {
    const mobileNavIcon = document.querySelector('.mobile__nav-icon');
    mobileNavIcon.addEventListener('click', function (e) {
        e.preventDefault();
        mobileNavIcon.classList.toggle('mobile__nav-icon--active');
        document.body.classList.toggle('mobile__nav--active');
    });

    if (matchMedia) {
        var mq = window.matchMedia("(min-width: 768px)");
        mq.addListener(widthChange);
        widthChange(mq);
    }
    function widthChange(mq) {
        if (mq.matches) {
            document.body.classList.remove('mobile__nav--active');
            mobileNavIcon.classList.remove('mobile__nav-icon--active');
        }
    }
};

comps.setupSlider = () => {
    const multiSlides = document.querySelector('.js_multislides');
    if( multiSlides ) {
        lory(multiSlides, {
            infinite: 4,
            slidesToScroll: 4,
            enableMouseEvents: true
        });
    }
};

comps.contactForm = () => {
    const triggers = document.querySelectorAll('.js-contact-trigger');
    const contactForm = document.querySelector('.contact');
    
    Array.prototype.forEach.call(triggers, function (trigger) {
        trigger.addEventListener('click', (e) => {
            e.preventDefault();
            if(contactForm.classList.contains('hide')) {
                contactForm.classList.remove('hide');
            } else {
                contactForm.classList.add('hide');
            }
        });
    });
};




module.exports = comps;
