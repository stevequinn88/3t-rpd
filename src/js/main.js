// Node Modules
const domready = require('domready');
const fc = require('fastclick');
const $ = require('jquery');
const smoothScroll = require('smooth-scroll');

// requires
const components = require('./components/components.js');
const utilities = require('./utilities/utilities.js');
const video = require('./components/video.js');
const skrollr = require('skrollr');

//consts
//const winWidth = $(window).width();
let winHeight = $(window).height();

// Testing Payments
const test = true;
if (test) {
  // Test functions here
}

function videoWrapperSize() {
  const videoWrapper = $('.video--wrapper');
  videoWrapper.css("height", winHeight);
}

function bannerSize() {
    const banner = $('.banner');
    banner.css("height", winHeight);
}

function initialize() {

  //Plugins
  smoothScroll.init();

  // Components

  components.setupMobileMenu();
  components.contactForm();
  components.setupSlider();
  video.setupVideo();

  //Utilities
  utilities.setupNavWaypoint();

  videoWrapperSize();
  bannerSize();
  skrollr.init();
  
  $(window).on('resize', function(){
      winHeight = $(window).height();
      videoWrapperSize();
      bannerSize();
  });


}

// Attach to the global scope
window.finalScripts = {
  init : initialize
};

domready( () => {
  fc(document.body);
  finalScripts.init();
});
