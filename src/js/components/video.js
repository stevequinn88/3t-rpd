const video = {};

video.setupVideo = () => {
    // Video
    const video = document.getElementById("video");
    // Buttons
    var playButton = document.getElementById("play-pause");
    var muteButton = document.getElementById("mute");
    var fullScreenButton = document.getElementById("full-screen");
    // Sliders
    var seekBar = document.getElementById("seek-bar");
    var volumeBar = document.getElementById("volume-bar");

    video.muted = true;

    // Event listener for the play/pause button
    playButton.addEventListener("click", () => {
        if (video.paused == true) {
            // Play the video
            video.play();
            // Update the button text to 'Pause'
            playButton.innerHTML = '<svg class="icon icon__svg icon--small"><use xlink:href="#icon-pause"></svg>';
        } else {
            // Pause the video
            video.pause();
            // Update the button text to 'Play'
            playButton.innerHTML = '<svg class="icon icon__svg icon--small"><use xlink:href="#icon-play"></svg>';
        }
    });

    // Event listener for the mute button
    muteButton.addEventListener("click", () => {
        if (video.muted == false) {
            // Mute the video
            video.muted = true;
            // Update the button text
            muteButton.innerHTML = '<svg class="icon icon__svg icon--small"><use xlink:href="#icon-un-mute"></svg>';
        } else {
            // Unmute the video
            video.muted = false;
            // Update the button text
            muteButton.innerHTML ='<svg class="icon icon__svg icon--small"><use xlink:href="#icon-mute"></svg>';
        }
    });

    // Event listener for the full-screen button
    fullScreenButton.addEventListener("click", () => {
        if (video.requestFullscreen) {
            video.requestFullscreen();
        } else if (video.mozRequestFullScreen) {
            video.mozRequestFullScreen(); // Firefox
        } else if (video.webkitRequestFullscreen) {
            video.webkitRequestFullscreen(); // Chrome and Safari
        }
    });

    // Event listener for the seek bar
    seekBar.addEventListener("change", () => {
        // Calculate the new time
        const time = video.duration * (seekBar.value / 100);
        // Update the video time
        video.currentTime = time;
    });

    // Update the seek bar as the video plays
    video.addEventListener("timeupdate", () => {
        // Calculate the slider value
        const value = (100 / video.duration) * video.currentTime;
        // Update the slider value
        seekBar.value = value;
    });

    // Pause the video when the slider handle is being dragged
    seekBar.addEventListener("mousedown", () => {
        video.pause();
        playButton.innerHTML = '<svg class="icon icon__svg icon--small"><use xlink:href="#icon-play"></svg>';
    });

    // Play the video when the slider handle is dropped
    seekBar.addEventListener("mouseup", () => {
        video.play();
        playButton.innerHTML = '<svg class="icon icon__svg icon--small"><use xlink:href="#icon-pause"></svg>';

    });

    // Event listener for the volume bar
    volumeBar.addEventListener("change", () => {
        // Update the video volume
        video.volume = volumeBar.value;
    });

    video.addEventListener( "canplay", () => {
        video.play();
    });

};

module.exports = video;